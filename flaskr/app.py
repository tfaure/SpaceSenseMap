import os

from flask import Flask, session, redirect, request, flash, jsonify, make_response
from flask import render_template
# from flask_session import Session
import requests

import json

# create and configure the app
app = Flask(__name__, instance_relative_config=True)
app.config.from_mapping(
    SECRET_KEY=os.urandom(24),
)

# ensure the instance folder exists
try:
    os.makedirs(app.instance_path)
except OSError:
    pass

# a simple page that says hello
@app.route('/')
def hello():
    pload = {'username':'sample_user','password':'frL2LgNd6s'}
    r = requests.post('https://spacesense-api.ew.r.appspot.com/login',data = pload)
    r_dictionary = r.json()
    token = r_dictionary['token']
    return render_template('SpaceSenseIFrameTest.html', token = token)

@app.route('/map')
def map():
    if request.args.get('token'):
        token = request.args.get('token')
        return render_template('SpaceSenseMap.html', token = token)
    else:
        return "ERROR no valid token"

@app.route('/deleteField')
def deleteField():
    if request.args.get('token'):
        token = request.args.get('token')
        fieldName = request.args.get('fieldName')
        print(fieldName);
        r = requests.get('https://spacesense-api.ew.r.appspot.com/fields/delete/' + fieldName + '?token=' + token)
        print(r.text);
        if r.ok:
            r_dictionary = r.json()
            # print (r_dictionary['message'])
            print(r_dictionary)
            return jsonify(r_dictionary)
    return "ERROR", 300

@app.route('/listFiles')
def listFiles():
    if request.args.get('token'):
        token = request.args.get('token')
        fieldName = request.args.get('fieldName')
        print(fieldName);
        r = requests.post('https://spacesense-api.ew.r.appspot.com/files/list?token=' + token + '&field_name=' + fieldName)
        print(r.text);
        if r.ok:
            r_dictionary = r.json()
            # print (r_dictionary['message'])
            print(r_dictionary)
            return jsonify(r_dictionary)
    return "ERROR", 300

@app.route('/addField', methods = ['POST'])
def addField():
    response = "start"
    if request.method == 'POST':
        token = request.form['token']
        shape = request.form['shape']
        name = request.form['name']
        label = request.form['label']
        jsonString = '{"field_name":"' + name + '","label":[' + label + '],"geojson":{"type": "FeatureCollection","features": [{"type": "Feature","properties": {},' + shape + '}]}}';
        print (jsonString)
        r = requests.post('https://spacesense-api.ew.r.appspot.com/fields/register?token=' + token, json = json.loads(jsonString))
        r_dictionary = r.json()
        print (r_dictionary['message'])
        flash(r_dictionary['message'])
    return redirect("/map?token="+token , code=302)

@app.route('/listFields', methods=['GET', 'POST'])
def listFields():
    response = "start"
    if request.args.get('token'):
        token = request.args.get('token')
        print(token)
        r = requests.post('https://spacesense-api.ew.r.appspot.com/fields/list?token=' + token)
        if r.ok:
            r_dictionary = r.json()
            # print (r_dictionary['message'])
            print(r_dictionary)
            return jsonify(r_dictionary)
    return "ERROR"

if __name__ == '__main__':
    app.run()
