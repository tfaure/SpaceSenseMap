import os

class BaseConfig(object):
    DEBUG = True
    SECRET_KEY = os.urandom(24)