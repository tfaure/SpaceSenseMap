"use strict";

// Get the modal element
var modal = document.getElementById("newFieldModal");
var bottomModal = document.getElementById("bottomModal");
// Get the button that opens the modal
var bottomModalBtn = document.getElementById("bottomModalBtn");
var spanBottomModal = document.getElementById("closeBottomModal");
let map;
let infoWindow;

// newField is the field drawn and shown on the map
var newField;


function initMap() {
  
  // init map
  map = new google.maps.Map(document.getElementById("map"), {
    // the location where the map will start
    center: {
      lat: 46.0344,
      lng: 4.0727
    },
    // zoom level
    zoom: 11,
    mapTypeId: 'satellite',
    mapTypeControl : false,
    streetViewControl: false,
    fullscreenControl: false,
  });

  // drawing manager is the tool used to draw shapes on the map
  var drawingManager = new google.maps.drawing.DrawingManager({
    // select no drawing mode by default
    drawingMode: google.maps.drawing.OverlayType.NULL,
    drawingControl: true,
    drawingControlOptions: {
      position: google.maps.ControlPosition.RIGHT_TOP,
      drawingModes: ['polygon']
    },
    polygonOptions: {
      fillColor: '#13235B',
      fillOpacity: 0.4,
      strokeWeight: 5,
      clickable: true,
      editable: true,
      zIndex: 1
    },
  });
  // assign the drawing manager to the previously initialized map
  drawingManager.setMap(map);

  // when a polygon is drawn execute a function
  google.maps.event.addListener(drawingManager, 'polygoncomplete', function(poly) {
    // gives the value of the newly finished shape to the newField variable
    newField = poly;
    // get the path of the newly drawn Polygon
    const vertices = poly.getPath();
    // fill the form with a GEOjson
    addShapeToForm(vertices);
    // go over every point in the polygon 
    poly.getPaths().forEach(function(path, index){ 
      
      //  When an event gets triggered, updates the form for registering new fields 
      //  also update the newField variable

      google.maps.event.addListener(path, 'insert_at', function(poly){
        // Point was added
        addShapeToForm(path);
      });
    
      google.maps.event.addListener(path, 'remove_at', function(poly){
        // Point was removed
        addShapeToForm(path);
      });
    
      google.maps.event.addListener(path, 'set_at', function(){
        // Point was moved
        addShapeToForm(path);
      });
    });
    // get out of drawing mode
    drawingManager.setOptions({
      drawingMode: null,
    });
    // display the add new field button
    newFieldBtn.style.display = "block";
  });
}

 // function that takes a path in argument and adds this path to the form that will be sent to the API to register a new field
function addShapeToForm(vertices) {
  // update the newField with it's new path
  newField.setOptions({path: vertices});
  // format the string to be geojson friendly
  let contentString = "\"geometry\":{ \"type\": \"Polygon\",\"coordinates\": [[";
  var xy
  // Iterate over the vertices.
  for (let i = 0; i < vertices.getLength(); i++) {
    xy = vertices.getAt(i);
    contentString +=
      "[" + xy.lat() + "," + xy.lng() + "],";
  }
  // adds the first vertice as the last (not mendatory)
  xy = vertices.getAt(0);
  contentString += "[" + xy.lat() + "," + xy.lng() + "]";
  // Closes the Json string
  contentString += "]]}"
  document.getElementById("shape").innerHTML = contentString;
}
// execute a JS function before submiting the form
$('#submit').on("click", function () {
  const label = $("#label").val(); // get label value
  if (label != null)
  {
    var a_label = label.split(','); // create an array from the labels
    var jsonStringLabel = ""; // init the new string
    a_label.forEach(function(value, index, array)
    {
      if (value.startWith('"') && value.endsWith('"')) // check if the parameter is already between quotes
        jsonStringLabel += value  // adds the new label to the string as is
      else
        jsonStringLabel += '"' + value + '"' // adds the new label to the string with some quotes
      // adds a comma after a new label except the last one
      if (index != array.length - 1)
        jsonStringLabel += ','  
    });
    // add the new label string to the form
    document.getElementById("label").value = jsonStringLabel
  }
  // submit the form
  $("#form")[0].submit();
});

// when the user click the cancel button, closes the modal
$("#close").on("click", function() {
  modal.style.display = "none";
  newField.setMap(null);
});

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
    newField.setMap(null);
  }
  if (event.target == bottomModal) {
    bottomModal.style.display = "none";
  }
}

// When the user clicks on <spanBottomModal> (x), close the modal
spanBottomModal.onclick = function() {
  bottomModal.style.display = "none";
}

// When the user clicks the button, open the modal 
bottomModalBtn.onclick = function() {
  bottomModal.style.display = "block";
}

// When the user clicks the button, open the modal 
newFieldBtn.onclick = function() {
  modal.style.display = "block";
  newFieldBtn.style.display = "none";
}
